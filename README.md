# Global Kinetic Assessment

App that consumes the [Open Weather Api](https://openweathermap.org)

## Screenshots

[<img src="/screenshots/weather_screen.png">](/screenshots/weather_screen.png)

## Installation
Clone this repository and import into **Android Studio**

```bash
git clone https://gitlab.com/shadracktl40/globalkinetic.git
```
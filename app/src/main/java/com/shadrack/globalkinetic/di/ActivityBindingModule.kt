package com.shadrack.globalkinetic.di

import com.shadrack.domain.di.ActivityScoped
import com.shadrack.globalkinetic.ui.weather.WeatherActivity
import com.shadrack.globalkinetic.ui.weather.WeatherModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = [WeatherModule::class])
    internal abstract fun weatherActivity(): WeatherActivity
}
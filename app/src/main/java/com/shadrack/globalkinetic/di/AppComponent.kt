package com.shadrack.globalkinetic.di

import com.shadrack.domain.di.module.DataModule
import com.shadrack.domain.di.module.DomainModule
import com.shadrack.globalkinetic.GlobalKineticApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import com.shadrack.domain.di.ViewModelModule
import javax.inject.Singleton

/**
 * Main component of the app, created and persisted in the Application class.
 *
 * Whenever a new module is created, it should be added to the list of modules.
 * [AndroidSupportInjectionModule] is the module from Dagger.Android that helps with the
 * generation and location of subcomponents.
 */
@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        DomainModule::class,
        DataModule::class
    ]
)
interface AppComponent : AndroidInjector<GlobalKineticApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<GlobalKineticApplication>()
}
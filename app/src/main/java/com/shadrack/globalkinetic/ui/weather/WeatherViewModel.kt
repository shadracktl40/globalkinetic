package com.shadrack.globalkinetic.ui.weather

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shadrack.domain.model.Coordinates
import com.shadrack.domain.model.Weather
import com.shadrack.domain.result.Result
import com.shadrack.domain.result.failed
import com.shadrack.domain.result.isLoading
import com.shadrack.domain.result.succeeded
import com.shadrack.domain.usecase.weather.GetWeatherUseCase
import com.shadrack.domain.util.map
import javax.inject.Inject

class WeatherViewModel @Inject constructor(
    private val getWeatherUseCase: GetWeatherUseCase
) : ViewModel() {
    private val weatherResult: MutableLiveData<Result<Weather>> = MutableLiveData()
    private val weatherResultObserver = MediatorLiveData<Weather>()

    val loading: LiveData<Boolean>
    val weather: LiveData<Weather> get() = weatherResultObserver
    val error: LiveData<Result.Error?>

    private var latitude: Double? = null
    private var longitude: Double? = null
    var hasDeniedPermission: Boolean = false

    init {
        // observe get weather request
        weatherResultObserver.addSource(weatherResult) {
            val weather = if (it.succeeded) (it as Result.Success).data else return@addSource
            weatherResultObserver.value = weather
        }

        // observe loading state
        loading = weatherResult.map {
            it.isLoading
        }

        // observe error state
        error = weatherResult.map {
            if (it.failed) it as Result.Error else null
        }
    }

    fun getWeather(latitude: Double, longitude: Double) {
        this.latitude = latitude
        this.longitude = longitude
        getWeatherUseCase(Coordinates(latitude, longitude), weatherResult)
    }

    fun refreshWeather() {
        if (latitude != null && longitude != null) {
            getWeatherUseCase(Coordinates(latitude!!, longitude!!), weatherResult)
        }
    }
}
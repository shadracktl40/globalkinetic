package com.shadrack.globalkinetic.ui.weather

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.lifecycle.Observer
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.shadrack.globalkinetic.R
import com.shadrack.globalkinetic.common.ViewModelActivity
import com.shadrack.globalkinetic.databinding.ActivityWeatherBinding
import com.shadrack.globalkinetic.utils.showSnackBar
import kotlin.reflect.KClass


class WeatherActivity : ViewModelActivity<ActivityWeatherBinding, WeatherViewModel>(),
    MultiplePermissionsListener {
    override fun getViewModel(): KClass<WeatherViewModel> = WeatherViewModel::class
    override fun getLayoutRes(): Int = R.layout.activity_weather

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        viewBinding.apply {
            lifecycleOwner = this@WeatherActivity
            viewModel = this@WeatherActivity.viewModel
        }

        viewModel.apply {
            error.observe(this@WeatherActivity, Observer { error ->
                error?.let {
                    showSnackBar(it.message)
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()

        when {
            hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION) && hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) -> {
                getLastKnownLocation()
            }
            !viewModel.hasDeniedPermission -> requestPermissions()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.weather_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.refresh -> {
                viewModel.refreshWeather()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun requestPermissions() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            .withListener(this)
            .withErrorListener {
                showSnackBar(it.name)
            }
            .check()
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    viewModel.getWeather(location.latitude, location.longitude)
                } ?: showSnackBar(getString(R.string.error_could_not_get_location))
            }

    }

    private fun showSnackBar(message: String) {
        showSnackBar(viewBinding.weatherCoordinator, message)
    }

    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
        report?.let {
            if (report.areAllPermissionsGranted()) {
                getLastKnownLocation()
            } else {
                viewModel.hasDeniedPermission = true
                showSnackBar(getString(R.string.error_location_permission_needed))
            }
        }
    }

    override fun onPermissionRationaleShouldBeShown(
        permissions: MutableList<PermissionRequest>?,
        token: PermissionToken?
    ) {
        token?.continuePermissionRequest()
    }
}
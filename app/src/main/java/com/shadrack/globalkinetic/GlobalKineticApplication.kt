package com.shadrack.globalkinetic

import com.shadrack.globalkinetic.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class GlobalKineticApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

    override fun onCreate() {
        super.onCreate()

        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.DEV_BUILD) {
            Timber.plant(Timber.DebugTree())
        } else {
            // plant production tree here. i.e crashlytics etc
        }
    }
}
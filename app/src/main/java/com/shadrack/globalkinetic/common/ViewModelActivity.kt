package com.shadrack.globalkinetic.common

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjection
import com.shadrack.domain.di.ViewModelFactory
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class ViewModelActivity<DB : ViewDataBinding, VM : ViewModel> : BaseActivity<DB>() {
    @Inject
    protected lateinit var viewModelFactory: ViewModelFactory
    protected lateinit var viewModel: VM

    abstract fun getViewModel(): KClass<VM>

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(getViewModel().java)
    }
}
package com.shadrack.globalkinetic.utils

import android.text.format.DateFormat
import android.view.View
import android.widget.ImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.snackbar.Snackbar
import com.shadrack.globalkinetic.R
import java.util.*

/** Show snack bar */
fun showSnackBar(view: CoordinatorLayout, message: String) {
    Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
}

/** Downloads an image on an [ImageView] */
@BindingAdapter("icon")
fun ImageView.setIcon(icon: String?) {
    icon?.let {
        Glide
            .with(this)
            .load(it)
            .centerCrop()
            .placeholder(R.mipmap.ic_launcher)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(this)
    }
}

/** Shows or hides a [View] */
@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(show: Boolean?) {
    visibility = if (show == true) View.VISIBLE else View.GONE
}

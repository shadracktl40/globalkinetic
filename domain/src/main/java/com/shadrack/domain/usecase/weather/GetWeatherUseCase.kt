package com.shadrack.domain.usecase.weather

import com.shadrack.domain.data.weather.IWeatherRepository
import com.shadrack.domain.model.Coordinates
import com.shadrack.domain.model.Weather
import com.shadrack.domain.usecase.UseCase
import javax.inject.Inject

class GetWeatherUseCase @Inject constructor(
    private val repository: IWeatherRepository
) : UseCase<Coordinates, Weather>() {
    override fun execute(parameters: Coordinates): Weather {
        return repository.getWeather(parameters)
    }
}
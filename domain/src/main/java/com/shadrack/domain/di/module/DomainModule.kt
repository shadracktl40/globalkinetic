package com.shadrack.domain.di.module

import com.shadrack.domain.BuildConfig
import com.shadrack.domain.api.ApiConstants.BASE_URL
import com.shadrack.domain.api.ApiConstants.CONNECT_READ_TIMEOUT
import com.shadrack.domain.util.GsonHelper
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class DomainModule {
    @Provides
    @Singleton
    internal fun providesRetrofit(): Retrofit {
        val okHttpClient = OkHttpClient.Builder().apply {
            // Set timeouts
            connectTimeout(CONNECT_READ_TIMEOUT, TimeUnit.SECONDS)
            readTimeout(CONNECT_READ_TIMEOUT, TimeUnit.SECONDS)

            // Add request and response logging
            if (BuildConfig.DEV_BUILD) {
                val logging = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
                    Timber.tag(OKHTTP_TAG).e(message)
                })
                // Switch to Level.BODY to get the Request/Response headers and body
                logging.level = HttpLoggingInterceptor.Level.BODY
                addInterceptor(logging)
            }
        }

        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonHelper.getGsonBuilder()))
            .client(okHttpClient.build()).build()
    }

    companion object {
        const val OKHTTP_TAG = "OkHttp"
    }
}
package com.shadrack.domain.di.module

import com.shadrack.domain.api.weather.IWeatherApiService
import com.shadrack.domain.data.weather.IWeatherDataSource
import com.shadrack.domain.data.weather.IWeatherRepository
import com.shadrack.domain.data.weather.WeatherDataSourceImpl
import com.shadrack.domain.data.weather.WeatherRepositoryImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class DataModule {
    @Provides
    @Singleton
    internal fun provideWeatherApiService(retrofit: Retrofit): IWeatherApiService {
        return retrofit.create(IWeatherApiService::class.java)
    }

    @Provides
    @Singleton
    internal fun provideWeatherDataSource(weatherApiService: IWeatherApiService): IWeatherDataSource {
        return WeatherDataSourceImpl(weatherApiService)
    }

    @Provides
    @Singleton
    internal fun provideWeatherRepository(weatherDataSource: IWeatherDataSource): IWeatherRepository {
        return WeatherRepositoryImpl(weatherDataSource)
    }
}
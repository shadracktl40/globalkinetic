package com.shadrack.domain.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken.*
import com.google.gson.stream.JsonWriter
import java.text.SimpleDateFormat
import java.util.*

object GsonHelper {
    private const val ISO8601_WEBSERVICE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"

    fun getGsonBuilder(): Gson {
        val builder = GsonBuilder()

        builder.setDateFormat(ISO8601_WEBSERVICE_FORMAT)

        builder.registerTypeAdapter(Boolean::class.java, mTolerantBooleanAdapter)
        builder.registerTypeAdapter(Integer::class.java, mTolerantIntAdapter)
        builder.registerTypeAdapter(Long::class.java, mTolerantLongAdapter)
        builder.registerTypeAdapter(Double::class.java, mTolerantDoubleAdapter)
        builder.registerTypeAdapter(String::class.java, mTolerantStringAdapter)
        builder.registerTypeAdapter(Date::class.java, mTolerantDateAdapter)

        return builder.create()
    }

    private val mTolerantBooleanAdapter = object : TypeAdapter<Boolean>() {
        override fun write(out: JsonWriter, value: Boolean?) {
            if (value == null) {
                out.nullValue()
            } else {
                out.value(value)
            }
        }

        override fun read(inObj: JsonReader): Boolean? {
            return when (inObj.peek()) {
                BOOLEAN -> inObj.nextBoolean()
                NUMBER -> {
                    val value = inObj.nextDouble()
                    if (value < 0 || value > 1) null else value > 0
                }
                STRING -> {
                    try {
                        inObj.nextString()!!.toBoolean()
                    } catch (e: Exception) {
                        null
                    }
                }
                else -> {
                    inObj.nextNull()
                    null
                }
            }
        }
    }

    private val mTolerantIntAdapter = object : TypeAdapter<Int>() {
        override fun write(out: JsonWriter, value: Int?) {
            if (value == null) {
                out.nullValue()
            } else {
                out.value(value)
            }
        }

        override fun read(inObj: JsonReader): Int? {
            return when (inObj.peek()) {
                BOOLEAN -> if (inObj.nextBoolean()) 1 else 0
                NUMBER -> Math.round(inObj.nextDouble()).toInt()
                STRING -> {
                    try {
                        inObj.nextString().toInt()
                    } catch (e: Exception) {
                        null
                    }
                }
                else -> {
                    inObj.nextNull()
                    null
                }
            }
        }
    }

    private val mTolerantLongAdapter = object : TypeAdapter<Long>() {
        override fun write(out: JsonWriter, value: Long?) {
            if (value == null) {
                out.nullValue()
            } else {
                out.value(value)
            }
        }

        override fun read(inObj: JsonReader): Long? {
            return when (inObj.peek()) {
                BOOLEAN -> if (inObj.nextBoolean()) 1L else 0L
                NUMBER -> inObj.nextDouble().toLong()
                STRING -> {
                    try {
                        inObj.nextString().toLong()
                    } catch (e: Exception) {
                        null
                    }
                }
                else -> {
                    inObj.nextNull()
                    null
                }
            }
        }
    }

    private val mTolerantDoubleAdapter = object : TypeAdapter<Double>() {
        override fun write(out: JsonWriter, value: Double?) {
            if (value == null) {
                out.nullValue()
            } else {
                out.value(value)
            }
        }

        override fun read(inObj: JsonReader): Double? {
            return when (inObj.peek()) {
                BOOLEAN -> if (inObj.nextBoolean()) 1.0 else 0.0
                NUMBER -> inObj.nextDouble()
                STRING -> {
                    try {
                        inObj.nextString().toDouble()
                    } catch (e: Exception) {
                        null
                    }
                }
                else -> {
                    inObj.nextNull()
                    null
                }
            }
        }
    }

    private val mTolerantStringAdapter = object : TypeAdapter<String>() {
        override fun write(out: JsonWriter, value: String?) {
            if (value == null) {
                out.nullValue()
            } else {
                out.value(value)
            }
        }

        override fun read(inObj: JsonReader): String? {
            return when (inObj.peek()) {
                BOOLEAN -> inObj.nextBoolean().toString()
                NUMBER -> inObj.nextDouble().toString()
                STRING -> inObj.nextString()
                else -> {
                    inObj.nextNull()
                    null
                }
            }
        }
    }

    private val mTolerantDateAdapter = object : TypeAdapter<Date>() {
        override fun write(out: JsonWriter, value: Date?) {
            if (value == null) {
                out.nullValue()
            } else {
                val df = SimpleDateFormat(ISO8601_WEBSERVICE_FORMAT, Locale.getDefault())
                out.value(df.format(value))
            }
        }

        override fun read(inObj: JsonReader): Date? {
            return when (inObj.peek()) {
                STRING -> {
                    val df = SimpleDateFormat(ISO8601_WEBSERVICE_FORMAT, Locale.getDefault())
                    df.parse(inObj.nextString())
                }
                else -> {
                    inObj.nextNull()
                    null
                }
            }
        }
    }
}

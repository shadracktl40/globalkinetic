package com.shadrack.domain.util

import android.text.format.DateFormat
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.shadrack.domain.api.ErrorResponse
import com.shadrack.domain.result.Result
import retrofit2.Call
import timber.log.Timber
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*

/**
 * Converts Unix time to Date
 */
fun Long.unixTimeToDate(): Date = Date(this * 1000)

/**
 * Converts Date to time String
 */
fun Date.toTimeString() = DateFormat.format("HH:mm", this).toString()

/** Uses `Transformations.map` on a LiveData */
fun <X, Y> LiveData<X>.map(body: (X) -> Y): LiveData<Y> {
    return Transformations.map(this, body)
}

/** Makes a synchronous Retrofit call */
fun <ResponseType : Any> Call<ResponseType>.fetch(): ResponseType {
    try {
        val response = this.execute()
        if (response.isSuccessful) {
            return response.body()!!
        } else {
            /** Try parsing error body to [ErrorResponse] */
            try {
                val errorResponse: ErrorResponse? = Gson()
                    .fromJson(response.errorBody()?.toString(), ErrorResponse::class.java)
                if (errorResponse != null) {
                    throw Result.ServerException(response.code(), errorResponse.message!!)
                } else {
                    throw Result.ServerException(response.code(), response.message())
                }
            } catch (exception: Exception) {
                throw Result.ServerException(response.code(), response.message())
            }
        }
    } catch (exception: Exception) {
        throw exception
    }
}

/** Exception helper for Crashlytics */
fun Exception.logException(): Exception {
    with(this) {
        when (this) {
            is Result.ServerException -> {
                if (code !in 200..300) {
                    logException("HTTP", "Status Code: $code", Exception(message))
                }
            }
            is NullPointerException -> {
                logException("NullPointerException", message, this)
            }
            is JsonParseException -> {
                logException("JsonParseException", message, this)
            }
            is ConnectException,
            is UnknownHostException -> {
                logException("ConnectException", message, this)
                return Result.NetworkException("Please check your internet settings and try again.")
            }
            is SocketTimeoutException -> {
                logException("SocketTimeoutException", message, this)
                return Result.NetworkException("Request timed out, please try again.")
            }
            else -> {
                logException("UnknownException", message, this)
            }
        }
        return this
    }
}

/** Logs an exception with Timber */
private fun logException(tag: String, message: String?, throwable: Throwable?) {
    Timber.tag(tag).e(throwable, message)
}
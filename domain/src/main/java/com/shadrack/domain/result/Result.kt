package com.shadrack.domain.result

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class Result<out R> {
    // Exception types
    class ServerException(val code: Int, val error: String) : Exception()

    class NetworkException(val error: String) : Exception()

    // Base Error
    abstract class Error(val message: String) : Result<Nothing>()

    // Result types
    class Loading(val isLoading: Boolean = true) : Result<Nothing>()

    class Loaded(val isLoaded: Boolean = true) : Result<Nothing>()

    data class Success<out T>(val data: T) : Result<T>()
    class Server(val code: Int, message: String) : Error(message)
    class Network(message: String) : Error(message)
    class Unknown(message: String) : Error(message)

    override fun toString(): String {
        return when (this) {
            is Loading -> "Loading[data=$isLoading]"
            is Loaded -> "Loaded[data=$isLoaded]"
            is Success<*> -> "Success[data=$data]"
            is Server -> "Server[code=$code, message=$message]"
            is Network -> "Network[message=$message]"
            is Unknown -> "Unknown[message=$message]"
            is Error -> "Error[message=$message]"
        }
    }
}

/**
 * `true` if [Result] is of type [Result.Loading] & [Result.Loading.isLoading] is `true`.
 */
val Result<*>.isLoading
    get() = this is Result.Loading && isLoading

/**
 * `true` if [Result] is of type [Result.Success] & holds non-null [Result.Success.data].
 */
val Result<*>.succeeded
    get() = this is Result.Success && data != null

/**
 * `true` if [Result] is of type [Result.Error].
 */
val Result<*>.failed
    get() = this is Result.Error
package com.shadrack.domain.api.weather

import com.shadrack.domain.api.weather.dto.*

data class WeatherApiResponse(
    val coord: CoordDTO?,
    val weather: List<WeatherDTO>?,
    val base: String?,
    val main: MainDTO?,
    val wind: WindDTO?,
    val clouds: CloudsDTO?,
    val dt: Long?,
    val sys: SysDTO?,
    val timezone: Int?,
    val id: Int?,
    val name: String?,
    val cod: Int?
)
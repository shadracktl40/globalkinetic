package com.shadrack.domain.api

import com.shadrack.domain.BuildConfig

object ApiConstants {
    const val CONNECT_READ_TIMEOUT = 60L
    private const val STAGING_URL = "https://api.openweathermap.org/"
    private const val LIVE_URL = "https://api.openweathermap.org/"
    const val API_KEY = "53f9d8e4213222cf517d86dc406d67fc"

    // Set base url based on flavor
    val BASE_URL = if (BuildConfig.DEV_BUILD || BuildConfig.STAGING) STAGING_URL else LIVE_URL

    fun getIconUrl(icon: String): String = "${BASE_URL.replace("api.", "")}img/wn/${icon}@4x.png"
}
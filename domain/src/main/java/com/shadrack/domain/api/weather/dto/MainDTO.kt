package com.shadrack.domain.api.weather.dto

import com.google.gson.annotations.SerializedName

data class MainDTO(
    val temp: Double?,
    @SerializedName("feels_like") val feelsLike: Double?,
    @SerializedName("temp_min") val tempMin: Double?,
    @SerializedName("temp_max") val tempMax: Double?,
    val pressure: Int?,
    val humidity: Int?
)
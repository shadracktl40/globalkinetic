package com.shadrack.domain.api

abstract class BaseResponse {
    val cod: String? = null
    val message: String? = null
}
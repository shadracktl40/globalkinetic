package com.shadrack.domain.api.weather.dto

data class SysDTO(
    val type: Int?,
    val id: Int?,
    val message: Double?,
    val country: String?,
    val sunrise: Long?,
    val sunset: Long?
)
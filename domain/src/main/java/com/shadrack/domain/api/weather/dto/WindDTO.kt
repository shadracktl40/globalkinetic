package com.shadrack.domain.api.weather.dto

data class WindDTO(val speed: Double?, val deg: Double?)
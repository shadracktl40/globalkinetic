package com.shadrack.domain.api

data class ErrorResponse(val cod: String? = null, val message: String? = null)
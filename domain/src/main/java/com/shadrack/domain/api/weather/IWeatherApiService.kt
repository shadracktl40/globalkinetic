package com.shadrack.domain.api.weather

import com.shadrack.domain.api.ApiConstants
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface IWeatherApiService {
    @GET("data/2.5/weather")
    fun getWeather(
            @Query("lat") latitude: Double,
            @Query("lon") longitude: Double,
            @Query("appid") appId: String = ApiConstants.API_KEY
    ): Call<WeatherApiResponse>
}
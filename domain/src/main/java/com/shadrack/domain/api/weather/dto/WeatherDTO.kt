package com.shadrack.domain.api.weather.dto

data class WeatherDTO(
    val id: Int?,
    val main: String?,
    val description: String?,
    val icon: String?
)
package com.shadrack.domain.api.weather.dto

data class CoordDTO(val lat: Double?, val lon: Double?)
package com.shadrack.domain.data.weather

import com.shadrack.domain.api.weather.IWeatherApiService
import com.shadrack.domain.api.weather.WeatherApiResponse
import com.shadrack.domain.util.fetch

interface IWeatherDataSource {
    fun getWeather(latitude: Double, longitude: Double): WeatherApiResponse
}

class WeatherDataSourceImpl(
    private val weatherApiService: IWeatherApiService
) : IWeatherDataSource {

    override fun getWeather(latitude: Double, longitude: Double): WeatherApiResponse {
        return weatherApiService.getWeather(latitude, longitude).fetch()
    }
}
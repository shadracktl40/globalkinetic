package com.shadrack.domain.data.weather

import com.shadrack.domain.api.ApiConstants
import com.shadrack.domain.model.Coordinates
import com.shadrack.domain.model.Weather
import com.shadrack.domain.model.mapper.*
import com.shadrack.domain.util.unixTimeToDate

interface IWeatherRepository {
    fun getWeather(coordinates: Coordinates): Weather
}

class WeatherRepositoryImpl(
    private val weatherDataSource: IWeatherDataSource
) : IWeatherRepository {
    override fun getWeather(coordinates: Coordinates): Weather {
        val weatherResult = weatherDataSource.getWeather(coordinates.latitude, coordinates.longitude)
        val weather = weatherResult.weather!!.first()

        return Weather(
            weather.main!!,
            weather.description!!,
            ApiConstants.getIconUrl(weather.icon!!),
            CoordinatesModelMapper.toModel(weatherResult.coord!!),
            MainModelMapper.toModel(weatherResult.main!!),
            WindModelMapper.toModel(weatherResult.wind!!),
            CloudsModelMapper.toModel(weatherResult.clouds!!),
            SysModelMapper.toModel(weatherResult.sys!!),
            weatherResult.name!!,
            weatherResult.dt!!.unixTimeToDate()
        )
    }
}
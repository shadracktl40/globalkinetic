package com.shadrack.domain.model

import java.util.*

data class Sys(
    val country: String,
    val sunrise: String,
    val sunset: String
)
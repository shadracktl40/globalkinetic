package com.shadrack.domain.model.mapper

import com.shadrack.domain.api.weather.dto.WindDTO
import com.shadrack.domain.model.Wind

object WindModelMapper : BaseModelMapper<WindDTO, Wind> {
    override val toModel: (dto: WindDTO) -> Wind
        get() = {
            Wind(
                it.speed!!,
                it.deg!!
            )
        }
}
package com.shadrack.domain.model.mapper

import com.shadrack.domain.api.weather.dto.MainDTO
import com.shadrack.domain.model.Main

object MainModelMapper : BaseModelMapper<MainDTO, Main> {
    override val toModel: (dto: MainDTO) -> Main
        get() = {
            Main(
                it.temp!!,
                it.feelsLike!!,
                it.tempMin!!,
                it.tempMax!!,
                it.pressure!!,
                it.humidity!!
            )
        }
}
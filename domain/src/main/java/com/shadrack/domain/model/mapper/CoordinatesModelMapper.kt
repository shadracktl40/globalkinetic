package com.shadrack.domain.model.mapper

import com.shadrack.domain.api.weather.dto.CoordDTO
import com.shadrack.domain.model.Coordinates

object CoordinatesModelMapper : BaseModelMapper<CoordDTO, Coordinates> {
    override val toModel: (dto: CoordDTO) -> Coordinates
        get() = {
            Coordinates(
                it.lat!!,
                it.lon!!
            )
        }
}
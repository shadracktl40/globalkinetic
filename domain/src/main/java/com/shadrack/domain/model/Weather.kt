package com.shadrack.domain.model

import java.util.*

data class Weather(
    val skyCondition: String,
    val description: String,
    val icon: String,
    val coordinates: Coordinates,
    val main: Main,
    val wind: Wind,
    val clouds: Clouds,
    val sys: Sys,
    val name: String,
    val dateTime: Date
)
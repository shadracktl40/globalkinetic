package com.shadrack.domain.model

data class Coordinates(val latitude: Double, val longitude: Double)
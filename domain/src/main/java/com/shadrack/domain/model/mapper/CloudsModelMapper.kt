package com.shadrack.domain.model.mapper

import com.shadrack.domain.api.weather.dto.CloudsDTO
import com.shadrack.domain.model.Clouds

object CloudsModelMapper : BaseModelMapper<CloudsDTO, Clouds> {
    override val toModel: (dto: CloudsDTO) -> Clouds
        get() = {
            Clouds(it.all!!)
        }
}
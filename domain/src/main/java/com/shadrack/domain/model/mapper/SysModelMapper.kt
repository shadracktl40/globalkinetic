package com.shadrack.domain.model.mapper

import com.shadrack.domain.api.weather.dto.SysDTO
import com.shadrack.domain.model.Sys
import com.shadrack.domain.util.toTimeString
import com.shadrack.domain.util.unixTimeToDate

object SysModelMapper : BaseModelMapper<SysDTO, Sys> {
    override val toModel: (dto: SysDTO) -> Sys
        get() = {
            Sys(
                it.country!!,
                it.sunrise!!.unixTimeToDate().toTimeString(),
                it.sunset!!.unixTimeToDate().toTimeString()
            )
        }
}
package com.shadrack.domain.model

data class Wind(val speed: Double, val degree: Double)
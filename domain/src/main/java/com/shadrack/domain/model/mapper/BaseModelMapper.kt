package com.shadrack.domain.model.mapper

internal interface BaseModelMapper<DTO, Model> {
    val toModel: (dto: DTO) -> Model
}